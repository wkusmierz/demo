package kusmierz.wlodzimierz.com.githubusers;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.anything;


@RunWith(AndroidJUnit4.class)
public class UITests
{
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void buttonShouldUpdateText(){

        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onData(anything()).inAdapterView(withId(R.id.listView_fragment_users_list)).atPosition(0)
                .check (matches(isDisplayed()))
                .perform(click());

        onView (withId (R.id.textView_fragment_user_name))
                .check (matches(isDisplayed()))
                .check(matches(withText("defunkt")));

        Espresso.pressBack();

        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onData(anything()).inAdapterView(withId(R.id.listView_fragment_users_list)).atPosition(1)
                .check (matches(isDisplayed()))
                .perform(click());

        onView (withId (R.id.textView_fragment_user_name))
                .check (matches(isDisplayed()))
                .check(matches(withText("pijhyett")));
    }
}
