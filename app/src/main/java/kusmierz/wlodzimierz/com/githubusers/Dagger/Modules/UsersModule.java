package kusmierz.wlodzimierz.com.githubusers.Dagger.Modules;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kusmierz.wlodzimierz.com.githubusers.Models.UserAll;
import kusmierz.wlodzimierz.com.githubusers.Models.UserCollection;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class UsersModule
{
    String mBaseUrl;


    public UsersModule(String baseUrl) {
        this.mBaseUrl = baseUrl;
    }

    @Provides
    @Singleton
    List<UserAll> provideArrayList() {

        List<UserAll> userAll = new ArrayList<>();
        return userAll;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


    @Provides
    @Singleton
    UserCollection provideUsersCollection(List<UserAll> usersList, Retrofit retrofit) {

        UserCollection userCollection = new UserCollection.Builder()
                .usersList(usersList)
                .retrofit(retrofit)
                .build();

        return userCollection;
    }
}
