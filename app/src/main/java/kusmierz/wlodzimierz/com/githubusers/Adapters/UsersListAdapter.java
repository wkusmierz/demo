package kusmierz.wlodzimierz.com.githubusers.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;
import kusmierz.wlodzimierz.com.githubusers.Fragments.UsersListFragment;
import kusmierz.wlodzimierz.com.githubusers.GithubApplication;
import kusmierz.wlodzimierz.com.githubusers.Models.UserAll;
import kusmierz.wlodzimierz.com.githubusers.R;
import kusmierz.wlodzimierz.com.githubusers.Utils.CircleTransform;

public class UsersListAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity activity;
    private List<UserAll> data;
    private static LayoutInflater inflater = null;


    public UsersListAdapter(Activity a, List<UserAll> d) {

        this.activity = a;
        this.data = d;
        this.inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {

        @BindView(R.id.textView_user_list_element) TextView name;
        @BindView(R.id.imageView_user_list_element) ImageView avatar;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }


    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;


        if (convertView == null) {

            vi = inflater.inflate(R.layout.user_list_element, null);
            holder = new ViewHolder(vi);
            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();


        holder.name.setText(data.get(position).getLogin());
        Picasso.with(vi.getContext()).load(data.get(position).getAvatarUrl()).transform(new CircleTransform()).into(holder.avatar);


        vi.setOnClickListener(new OnItemClickListener(position));
        return vi;
    }

    @Override
    public void onClick(View arg0) {}

    private class OnItemClickListener implements View.OnClickListener
    {
        private int mPosition;
        volatile int i = 0;


        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {

                MainActivity mainActivity = (MainActivity) activity;
                mainActivity.switchToUserFragment(data.get(mPosition).getLogin());
                Log.d("Clicked:", data.get(mPosition).getLogin());
        }
    }

}