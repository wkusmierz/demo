package kusmierz.wlodzimierz.com.githubusers.Models;

import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import kusmierz.wlodzimierz.com.githubusers.API.UsersAPI;
import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;
import kusmierz.wlodzimierz.com.githubusers.Adapters.UsersListAdapter;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class UserCollection
{
    private String userSelectedName;
    private List<UserAll> usersList;
    private Retrofit retrofit;
    private Subscription sub;

    private UserCollection(Builder builder)
    {
        this.userSelectedName = builder.userSelectedName;
        this.usersList = builder.usersList;
        this.retrofit = builder.retrofit;
        this.sub = builder.sub;
    }

    public static class Builder
    {
        private String userSelectedName;
        private List<UserAll> usersList;
        private Retrofit retrofit;
        private Subscription sub;

        public Builder(){}

        public Builder userSelectedName(String userSelectedName)
        {
            this.userSelectedName = userSelectedName;
            return this;
        }
        public Builder usersList(List<UserAll> usersList)
        {
            this.usersList = usersList;
            return this;
        }
        public Builder retrofit(Retrofit retrofit)
        {
            this.retrofit = retrofit;
            return this;
        }
        public Builder sub(Subscription sub)
        {
            this.sub = sub;
            return this;
        }
        public UserCollection build()
        {
            return new UserCollection(this);
        }

    }

    private void getPageById(final int id, final UsersListAdapter adapter, final MainActivity activity)
    {
        final UsersAPI apiService = retrofit.create(UsersAPI.class);
        Observable<List<UserAll>> allUsersObservable = apiService.getNextPageById(id);

        sub = allUsersObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserAll>>()
                {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e)
                    {
                        if(e.getMessage() != null)
                        {
                            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(List<UserAll> users)
                    {
                        usersList.addAll(users);
                        adapter.notifyDataSetChanged();
                    }
                });
    }


    public void loadUsers(final UsersListAdapter adapter, final MainActivity activity)
    {
        if(usersList.size() == 0)
        {
            getPageById(1, adapter, activity);
        }
        else
        {
            getPageById(usersList.get(usersList.size()-1).getId()+1, adapter, activity);
        }
    }

    public void getUser(final TextView textView, final MainActivity activity)
    {
        final UsersAPI apiService = retrofit.create(UsersAPI.class);
        Observable<User> userObservable = apiService.getUserByName(userSelectedName);

        sub = userObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>()
                {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e)
                    {
                        if(e.getMessage() != null)
                        {
                            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(User user)
                    {
                        textView.setText(user.getLogin());
                    }
                });
    }


    public Retrofit getRetrofit()
    {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit)
    {
        this.retrofit = retrofit;
    }

    public String getUserSelectedName()
    {
        return userSelectedName;
    }

    public void setUserSelectedName(String userSelectedName)
    {
        this.userSelectedName = userSelectedName;
    }

    public List<UserAll> getUsersList()
    {
        return usersList;
    }

    public void setUsersList(List<UserAll> usersList)
    {
        this.usersList = usersList;
    }

    public Subscription getSub()
    {
        return sub;
    }
}
