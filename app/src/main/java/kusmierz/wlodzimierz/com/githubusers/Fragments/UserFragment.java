package kusmierz.wlodzimierz.com.githubusers.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.com.githubusers.API.UsersAPI;
import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;
import kusmierz.wlodzimierz.com.githubusers.GithubApplication;
import kusmierz.wlodzimierz.com.githubusers.Models.User;
import kusmierz.wlodzimierz.com.githubusers.Models.UserCollection;
import kusmierz.wlodzimierz.com.githubusers.R;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UserFragment extends Fragment
{
    private View rootView;
    private MainActivity mainActivity = null;
    @BindView(R.id.textView_fragment_user_name)
    TextView userName;


    public UserFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this,rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mainActivity.getUsers().getUser(userName, mainActivity);
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(mainActivity.getUsers().getSub() != null && !mainActivity.getUsers().getSub().isUnsubscribed())
        {
            mainActivity.getUsers().getSub().unsubscribe();
        }
    }

    @Override
    public void onAttach(Context activity)
    {
        super.onAttach(activity);

        if (activity instanceof MainActivity)
        {
            mainActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

}