package kusmierz.wlodzimierz.com.githubusers.Dagger.Modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kusmierz.wlodzimierz.com.githubusers.GithubApplication;

@Module
public class AppModule {

    GithubApplication mApplication;

    public AppModule(GithubApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }
}