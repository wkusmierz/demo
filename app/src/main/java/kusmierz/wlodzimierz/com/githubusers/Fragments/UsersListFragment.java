package kusmierz.wlodzimierz.com.githubusers.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import kusmierz.wlodzimierz.com.githubusers.API.UsersAPI;
import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;
import kusmierz.wlodzimierz.com.githubusers.Adapters.UsersListAdapter;
import kusmierz.wlodzimierz.com.githubusers.GithubApplication;
import kusmierz.wlodzimierz.com.githubusers.Models.UserAll;
import kusmierz.wlodzimierz.com.githubusers.Models.UserCollection;
import kusmierz.wlodzimierz.com.githubusers.R;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class UsersListFragment extends Fragment
{
    private View rootView;
    private MainActivity mainActivity = null;
    private UsersListAdapter adapter = null;
    @BindView(R.id.listView_fragment_users_list)
    ListView listView;


    public UsersListFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_users_list, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        setupVariables();

        if(mainActivity.getUsers().getUsersList().size() == 0)
        mainActivity.getUsers().loadUsers(adapter, mainActivity);
    }



    private void setupVariables()
    {
        adapter = new UsersListAdapter(getActivity(), mainActivity.getUsers().getUsersList());
        listView.setAdapter(adapter);

        View footerView = ((LayoutInflater) rootView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_load_more, null, false);

        final Button loadMore = (Button) footerView.findViewById(R.id.fragment_users_list_button_load_more);
        loadMore.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                mainActivity.getUsers().loadUsers(adapter, mainActivity);
            }
        });

        listView.addFooterView(footerView);
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }


    @Override
    public void onPause()
    {
        super.onPause();

        if(mainActivity.getUsers().getSub() != null && !mainActivity.getUsers().getSub().isUnsubscribed())
        {
            mainActivity.getUsers().getSub().unsubscribe();
        }
    }


    @Override
    public void onAttach(Context activity)
    {
        super.onAttach(activity);

        if (activity instanceof MainActivity){
            mainActivity = (MainActivity) activity;
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

}
