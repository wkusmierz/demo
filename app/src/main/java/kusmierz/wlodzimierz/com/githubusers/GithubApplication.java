package kusmierz.wlodzimierz.com.githubusers;


import kusmierz.wlodzimierz.com.githubusers.Dagger.Components.AppComponent;
import kusmierz.wlodzimierz.com.githubusers.Dagger.Components.DaggerAppComponent;
import kusmierz.wlodzimierz.com.githubusers.Dagger.Modules.AppModule;
import kusmierz.wlodzimierz.com.githubusers.Dagger.Modules.UsersModule;


public class GithubApplication extends android.app.Application
{

    private AppComponent appComponent;
    public String BASE_URL = "https://api.github.com/";

    @Override
    public void onCreate()
    {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .usersModule(new UsersModule(BASE_URL))
                .build();
    }


    public AppComponent getAppComponent()
    {
        return appComponent;
    }

}
