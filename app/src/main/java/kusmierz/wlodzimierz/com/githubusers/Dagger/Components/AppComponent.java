package kusmierz.wlodzimierz.com.githubusers.Dagger.Components;

import javax.inject.Singleton;
import dagger.Component;
import kusmierz.wlodzimierz.com.githubusers.Activities.MainActivity;
import kusmierz.wlodzimierz.com.githubusers.Dagger.Modules.AppModule;
import kusmierz.wlodzimierz.com.githubusers.Dagger.Modules.UsersModule;

@Singleton
@Component(modules={AppModule.class, UsersModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    //void inject(UsersListFragment fragment);
    //void inject(UserFragment fragment);
    // void inject(MyService service);
}