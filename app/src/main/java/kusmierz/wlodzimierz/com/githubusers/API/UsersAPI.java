package kusmierz.wlodzimierz.com.githubusers.API;

import java.util.List;

import kusmierz.wlodzimierz.com.githubusers.Models.User;
import kusmierz.wlodzimierz.com.githubusers.Models.UserAll;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface UsersAPI
{
    @GET("users")
    Observable<List<UserAll>> getNextPageById(
            @Query("since") Integer id);

    @GET("users/{username}")
    Observable<User> getUserByName(@Path("username") String username);
}