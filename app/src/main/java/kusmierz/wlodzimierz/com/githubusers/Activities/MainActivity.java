package kusmierz.wlodzimierz.com.githubusers.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import kusmierz.wlodzimierz.com.githubusers.Fragments.UserFragment;
import kusmierz.wlodzimierz.com.githubusers.Fragments.UsersListFragment;
import kusmierz.wlodzimierz.com.githubusers.GithubApplication;
import kusmierz.wlodzimierz.com.githubusers.Models.UserCollection;
import kusmierz.wlodzimierz.com.githubusers.R;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    private Fragment fragment = null;
    private FragmentManager fragmentManager = null;
    @Inject
    UserCollection users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((GithubApplication) getApplication()).getAppComponent().inject(this);

        switchToUsersListFragment();
    }

    private void switchToUsersListFragment()
    {
        fragment = new UsersListFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainActivityLayout, fragment);
        fragmentTransaction.commit();
    }

    public void switchToUserFragment(String userSelectedName)
    {
            users.setUserSelectedName(userSelectedName);
            Log.d("test","test");
            Log.d("userSelectedName", users.getUserSelectedName());

            fragment = new UserFragment();
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainActivityLayout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
    }


    public UserCollection getUsers()
    {
        return users;
    }
}
